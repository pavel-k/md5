import math

#Func
def hexAdd(hex1, hex2):
    return hex(int(hex1, 16) + int(hex2, 16))

def leftShift(hex1, n):
    b = bin(int(hex1, 16))
    b = b.replace("-", "")
    b = b.replace("0b", "")

    tmp = b
    for i in range(0,n):
        b = b[1:]
        b += tmp[0]
        tmp = b

    return hex(int(b,2))

def func(num, x, y, z):
    x = int(x, 16)
    y = int(y, 16)
    z = int(z, 16)

    return {
        0: hex((x & y) | (~x & z)),
        1: hex((x & z) | (~z & y)),
        2: hex(x ^ y ^ z),
        3: hex(y ^ (~z | x)),
    }[num]

def funcConversion(num, a, b, c, d, Xk, Ti, s):
    Xk = hex(int(Xk, 2))
    Ti = hex(Ti)

    # a = b + ((a + F(b,c,d) + X[k] + T[i]) <<< s)
    r1 = hexAdd(a, func(num, b, c, d)) # a + F(b,c,d)
    r2 = hexAdd(r1, Xk)                # a + F(b,c,d) + X[k]
    r3 = hexAdd(r2, Ti)                # a + F(b,c,d) + X[k] + T[i]
    r4 = leftShift(r3, s)              # (a + F(b,c,d) + X[k] + T[i]) <<< s
    a = hexAdd(b, r4)                  # result

    return a

#Init
inputNum = 123
inputBits = bin(inputNum)

#Step 1
inputBits += '1'
while len(inputBits) % 512 != 448 % 512:
    inputBits += '0'

#Step 2
inputBits += inputBits[-32:]
inputBits += inputBits[-64:-32]

#Step 3
A = "0x67452301"
B = "0xEFCDAB89"
C = "0x98BADCFE"
D = "0x10325476"
T = [int(4294967296 * math.fabs(math.sin(x+1))) for x in range(64)]

#Step 4
X = [0]*16

for i in range(0, len(inputBits), 512):     #Split bits array of 512
    for j in range(0, 16):                  #Split 512 bits array of 32
        X[j] = inputBits[j*32:(j+1)*32]

    AA = A
    BB = B
    CC = C
    DD = D

    #Step 1
    #[abcd k s i] a = b + ((a + F(b,c,d) + X[k] + T[i]) <<< s). */
    #[ABCD  0 7  1][DABC  1 12  2][CDAB  2 17  3][BCDA  3 22  4]
    #[ABCD  4 7  5][DABC  5 12  6][CDAB  6 17  7][BCDA  7 22  8]
    #[ABCD  8 7  9][DABC  9 12 10][CDAB 10 17 11][BCDA 11 22 12]
    #[ABCD 12 7 13][DABC 13 12 14][CDAB 14 17 15][BCDA 15 22 16]

    A = funcConversion(0, A, B, C, D, X[0], T[7],  1)
    D = funcConversion(0, D, A, B, C, X[1], T[12], 2)
    C = funcConversion(0, C, D, A, B, X[2], T[17], 3)
    B = funcConversion(0, B, C, D, A, X[3], T[22], 4)

    A = funcConversion(0, A, B, C, D, X[4], T[7],  5)
    D = funcConversion(0, D, A, B, C, X[5], T[12], 6)
    C = funcConversion(0, C, D, A, B, X[6], T[17], 7)
    B = funcConversion(0, B, C, D, A, X[7], T[22], 8)

    A = funcConversion(0, A, B, C, D, X[8], T[7],  9)
    D = funcConversion(0, D, A, B, C, X[9], T[12], 10)
    C = funcConversion(0, C, D, A, B, X[10], T[17], 11)
    B = funcConversion(0, B, C, D, A, X[11], T[22], 12)

    A = funcConversion(0, A, B, C, D, X[12], T[7],  13)
    D = funcConversion(0, D, A, B, C, X[13], T[12], 14)
    C = funcConversion(0, C, D, A, B, X[14], T[17], 15)
    B = funcConversion(0, B, C, D, A, X[15], T[22], 16)

    #Step 2
    # [abcd k s i] a = b + ((a + G(b,c,d) + X[k] + T[i]) <<< s)
    # [ABCD  1 5 17][DABC  6 9 18][CDAB 11 14 19][BCDA  0 20 20]
    # [ABCD  5 5 21][DABC 10 9 22][CDAB 15 14 23][BCDA  4 20 24]
    # [ABCD  9 5 25][DABC 14 9 26][CDAB  3 14 27][BCDA  8 20 28]
    # [ABCD 13 5 29][DABC  2 9 30][CDAB  7 14 31][BCDA 12 20 32]

    A = funcConversion(1, A, B, C, D, X[1],  T[5],  17)
    D = funcConversion(1, D, A, B, C, X[6],  T[9],  18)
    C = funcConversion(1, C, D, A, B, X[11], T[14], 19)
    B = funcConversion(1, B, C, D, A, X[0],  T[20], 20)

    A = funcConversion(1, A, B, C, D, X[5],  T[5],  21)
    D = funcConversion(1, D, A, B, C, X[10], T[9],  22)
    C = funcConversion(1, C, D, A, B, X[15], T[14], 23)
    B = funcConversion(1, B, C, D, A, X[4],  T[20], 24)

    A = funcConversion(1, A, B, C, D, X[9],  T[5],  25)
    D = funcConversion(1, D, A, B, C, X[14], T[9],  26)
    C = funcConversion(1, C, D, A, B, X[3],  T[14], 27)
    B = funcConversion(1, B, C, D, A, X[8],  T[20], 28)

    A = funcConversion(1, A, B, C, D, X[13], T[5],  29)
    D = funcConversion(1, D, A, B, C, X[2],  T[9],  30)
    C = funcConversion(1, C, D, A, B, X[7],  T[14], 31)
    B = funcConversion(1, B, C, D, A, X[12], T[20], 32)

    #Step 3
    # [abcd k s i] a = b + ((a + H(b,c,d) + X[k] + T[i]) <<< s)
    # [ABCD  5 4 33][DABC  8 11 34][CDAB 11 16 35][BCDA 14 23 36]
    # [ABCD  1 4 37][DABC  4 11 38][CDAB  7 16 39][BCDA 10 23 40]
    # [ABCD 13 4 41][DABC  0 11 42][CDAB  3 16 43][BCDA  6 23 44]
    # [ABCD  9 4 45][DABC 12 11 46][CDAB 15 16 47][BCDA  2 23 48]

    A = funcConversion(2, A, B, C, D, X[5],  T[4],  33)
    D = funcConversion(2, D, A, B, C, X[8],  T[11], 34)
    C = funcConversion(2, C, D, A, B, X[11], T[16], 35)
    B = funcConversion(2, B, C, D, A, X[14], T[23], 36)

    A = funcConversion(2, A, B, C, D, X[1],  T[4],  37)
    D = funcConversion(2, D, A, B, C, X[4],  T[11], 38)
    C = funcConversion(2, C, D, A, B, X[7],  T[16], 39)
    B = funcConversion(2, B, C, D, A, X[10], T[23], 40)

    A = funcConversion(2, A, B, C, D, X[13], T[4],  41)
    D = funcConversion(2, D, A, B, C, X[0],  T[11], 42)
    C = funcConversion(2, C, D, A, B, X[3],  T[16], 43)
    B = funcConversion(2, B, C, D, A, X[6],  T[23], 44)

    A = funcConversion(2, A, B, C, D, X[9],  T[4],  45)
    D = funcConversion(2, D, A, B, C, X[12], T[11], 46)
    C = funcConversion(2, C, D, A, B, X[15], T[16], 47)
    B = funcConversion(2, B, C, D, A, X[2],  T[23], 48)

    #Step 4
    # [abcd k s i] a = b + ((a + I(b,c,d) + X[k] + T[i]) <<< s)
    # [ABCD  0 6 49][DABC  7 10 50][CDAB 14 15 51][BCDA  5 21 52]
    # [ABCD 12 6 53][DABC  3 10 54][CDAB 10 15 55][BCDA  1 21 56]
    # [ABCD  8 6 57][DABC 15 10 58][CDAB  6 15 59][BCDA 13 21 60]
    # [ABCD  4 6 61][DABC 11 10 62][CDAB  2 15 63][BCDA  9 21 64]

    A = funcConversion(3, A, B, C, D, X[0],  T[6],  49)
    D = funcConversion(3, D, A, B, C, X[7],  T[10], 50)
    C = funcConversion(3, C, D, A, B, X[14], T[15], 51)
    B = funcConversion(3, B, C, D, A, X[5],  T[21], 52)

    A = funcConversion(3, A, B, C, D, X[12], T[6],  53)
    D = funcConversion(3, D, A, B, C, X[3],  T[10], 54)
    C = funcConversion(3, C, D, A, B, X[10], T[15], 55)
    B = funcConversion(3, B, C, D, A, X[1],  T[21], 56)

    A = funcConversion(3, A, B, C, D, X[8],  T[6],  57)
    D = funcConversion(3, D, A, B, C, X[15], T[10], 58)
    C = funcConversion(3, C, D, A, B, X[6],  T[15], 59)
    B = funcConversion(3, B, C, D, A, X[13], T[21], 60)

    A = funcConversion(3, A, B, C, D, X[4],  T[6],  61)
    D = funcConversion(3, D, A, B, C, X[11], T[10], 62)
    C = funcConversion(3, C, D, A, B, X[2],  T[15], 63)
    B = funcConversion(3, B, C, D, A, X[9],  T[21], 64)

    A = hexAdd(AA, A)
    B = hexAdd(BB ,B)
    C = hexAdd(CC ,C)
    D = hexAdd(DD ,D)

#Result
print("%s %s %s %s" % (A, B, C, D))



#Test a = b + ((a + F(b,c,d) + X[k] + T[i]) <<< s)
# A = "0x67452301"
# B = "0xEFCDAB89"
# C = "0x98BADCFE"
# D = "0x10325476"
#
#
# r1 = hexAdd(A, func(0, B, C, D)) # a + F(b,c,d)
# print(r1)

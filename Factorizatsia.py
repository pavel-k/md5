from math import log

p = 5 #1747
q = 7 #1753
N = p * q

i = 0
result = 1
logN = log(N, 2)

while result != N and i < logN:
    result = (2**i) % N
    result *= 2
    if result > N: result %= N
    i += 1

